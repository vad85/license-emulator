package com.broker.emulator.controller;

import com.broker.emulator.service.EmulatorService;
import com.broker.emulator.xml.JobConfig;
import com.broker.emulator.xml.JobEmulator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/")
public class EmulatorController {

    @Autowired
    private EmulatorService emulatorService;

    @GetMapping(produces = "application/json")
    public JobConfig ok() {
        return this.emulatorService.getJobConfig();
    }

    @GetMapping(value = "start", produces = "application/json")
    public String run() throws IOException, InterruptedException {
        List<JobEmulator> jobEmulatorList = this.emulatorService.getJobConfig().getEmulator();
        for (JobEmulator jobEmulator: jobEmulatorList) {
            this.emulatorService.runServerEmulator(jobEmulator);
        }
        return "ok";
    }
}
