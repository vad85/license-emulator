package com.broker.emulator.service;

import com.broker.emulator.xml.JobConfig;
import com.broker.emulator.xml.JobEmulator;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.URL;
import java.nio.file.*;
import java.nio.file.attribute.PosixFilePermission;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Slf4j
@Service
public class EmulatorService {

    private Jaxb2Marshaller marshaller;

    Map<String, Process> runningServers = new HashMap<>();

    private String jobConfigPath = "job.xml";

    private String serverScript;
    private String statScript;

    public EmulatorService(Jaxb2Marshaller marshaller ) {
        this.marshaller = marshaller;

        String os = getOS();
        this.serverScript = readResource(String.format("emulator/%s/server", os));
        this.statScript = readResource(String.format("emulator/%s/stat", os));

        String jobxml = System.getProperty("jobpath");
        if(jobxml != null) {
            log.info("Load external config");
            this.jobConfigPath = jobxml;
        }
    }

    public JobConfig getJobConfig() {
        return getJobConfig(jobConfigPath);
    }

    public JobConfig getJobConfig(String filePath) {
        try {
            return fromXML(filePath, JobConfig.class);
        } catch (IOException ign) {}
        return null;
    }

    @PostConstruct
    public void run() {
        JobConfig jobConfig = this.getJobConfig();
        if (jobConfig != null) {
            jobConfig.getEmulator().forEach(jobEmulator -> {
                CompletableFuture.runAsync(() -> {
                    try {
                        this.runServerEmulator(jobEmulator);
                    } catch (IOException | InterruptedException e) {
                        log.error("ERROR: {}", e.getMessage());
                    }
                });
            });
        }
    }

    @Async
    public void runServerEmulator(JobEmulator jobEmulator) throws IOException, InterruptedException {
        log.info("Start license server emulator: {}", jobEmulator.getProgramPath());
        destroyIfPresent(jobEmulator.getProgramPath());

        //1. Prepare path & log directories
        Path emulatorDirectory = this.prepareEmulatorDirectory(jobEmulator);

        //2. Prepare services
        this.makeServices(jobEmulator, emulatorDirectory);

        //3. run
        this.runServerEmulatorProcess(jobEmulator);
    }

    private void destroyIfPresent(String programPath) throws InterruptedException {
        if (this.runningServers.containsKey(programPath)) {
            Process process = this.runningServers.get(programPath).destroyForcibly();
            process.waitFor();
            this.runningServers.remove(programPath);
        }
    }

    private <T> T fromXML(String path, Class<T> clazz) throws IOException {
        InputStream file = null;
        try {
            file = Files.newInputStream(Paths.get(path));
        } catch (IOException e) {}

        if (file == null) {
            file = this.getClass().getClassLoader().getResourceAsStream(path);
        }
        return (T) this.marshaller.unmarshal(new StreamSource(file));
    }

//    private  <T> T convertFromXMLToObject(InputStream is, Class<T> clazz) throws IOException {
//        InputStream is = null;
//        try {
//            is = this.getClass().getClassLoader().getResourceAsStream(xmlfile);
//            return (T) this.marshaller.unmarshal(new StreamSource(is));
//        } finally {
//            if (is != null) {
//                is.close();
//            }
//        }
//    }

    private String readResource(String path) {
        try {
            URL url = Resources.getResource(path);
            return Resources.toString(url, Charsets.UTF_8);
        }catch (IOException e) {
            log.error(e.getMessage());
        }
        return null;
    }

    private Path prepareEmulatorDirectory(JobEmulator jobEmulator) throws IOException {
        log.info("PrepareEmulatorDirectory: {}", jobEmulator.getProgramPath());
        String dir = jobEmulator.getProgramPath().substring(0, jobEmulator.getProgramPath().lastIndexOf(File.separator));
        Path dirPath = Files.createDirectories(Paths.get(dir));

        //Create default log file
        Files.write(Paths.get(jobEmulator.getLogPath()), "This is a default log file".getBytes());
        return dirPath;
    }

    private void makeServices(JobEmulator jobEmulator, Path appPath) throws IOException {
        log.info("makeServices for {}", getOS());
        //Server service
        String exeName = jobEmulator.getProgramPath().substring(jobEmulator.getProgramPath().lastIndexOf(File.separator) + 1);
        if (SystemUtils.IS_OS_LINUX) {

            Path serverFilePath = Paths.get(jobEmulator.getProgramPath());
            Path nc = serverFilePath.getParent().resolve("nc");

            String serverData = this.serverScript
                    .replace("${servicename}", exeName)
                    .replace("${port}", String.valueOf(jobEmulator.getPort()))
                    .replace("${nc}", nc.toAbsolutePath().toString())
                    .replace("${logpath}", jobEmulator.getLogPath());

            Files.write(serverFilePath, serverData.getBytes());
            Files.copy(Paths.get("/bin/nc"), nc, StandardCopyOption.REPLACE_EXISTING);

            //Stat service
            String statData = this.statScript
                    .replace("${text}", jobEmulator.getSampleOutput());
            Path statFilePath = Paths.get(jobEmulator.getSampleCommand());
            Files.write(statFilePath, statData.getBytes());

            //Set perm
            Set<PosixFilePermission> perms = new HashSet<>();
            perms.add(PosixFilePermission.OWNER_READ);
            perms.add(PosixFilePermission.OWNER_WRITE);
            perms.add(PosixFilePermission.OWNER_EXECUTE);
            perms.add(PosixFilePermission.GROUP_READ);
            perms.add(PosixFilePermission.OTHERS_READ);


            Files.setPosixFilePermissions(serverFilePath, perms);
            Files.setPosixFilePermissions(statFilePath, perms);
        } else if (SystemUtils.IS_OS_WINDOWS) {
            URL inputUrl = getClass().getClassLoader().getResource("emulator/windows/nc.exe");
            Path nc = Paths.get(jobEmulator.getProgramPath());
//            File dest = new File(nc.toFile());
            FileUtils.copyURLToFile(inputUrl, nc.toFile());

            String serverData = this.serverScript
                    .replace("${servicename}", exeName)
                    .replace("${nc}", nc.toAbsolutePath().toString())
                    .replace("${port}", String.valueOf(jobEmulator.getPort()))
                    .replace("${logpath}", jobEmulator.getLogPath());
            Path serverFilePathBat = Paths.get(jobEmulator.getProgramPath() + ".bat");
            Files.write(serverFilePathBat, serverData.getBytes(), StandardOpenOption.CREATE);

            String statData = this.statScript
                    .replace("${text}", jobEmulator.getSampleOutput());
            Path statFilePathData = Paths.get(jobEmulator.getSampleCommand()).getParent().resolve("sample.txt");
            Files.write(statFilePathData, jobEmulator.getSampleOutput().getBytes());

            Path statFileBat = Paths.get(jobEmulator.getSampleCommand());
            Files.write(statFileBat, String.format("@echo off\ncmd /c type %s", statFilePathData.toAbsolutePath().toString()).getBytes());



        }
    }

    public void runServerEmulatorProcess(JobEmulator jobEmulator) throws IOException, InterruptedException {
        List<String> args = new ArrayList<>();
        args.addAll(jobEmulator.getArgs().stream()
                .map(arg-> arg.getName() + " " + arg.getValue())
                .collect(Collectors.toList()));
        String argument = StringUtils.join(args.toArray(new String[args.size()]), " ");

        Runtime rt = Runtime.getRuntime();
        Process pr = null;
        if (SystemUtils.IS_OS_WINDOWS) {
            log.info("Run command: {}", jobEmulator.getProgramPath() + ".bat " + argument);
            pr = rt.exec(jobEmulator.getProgramPath() + ".bat " + argument);
        } else {
            log.info("Run command: {}", jobEmulator.getProgramPath() + " " + argument);
            pr = rt.exec(jobEmulator.getProgramPath() + " " + argument);
        }

        this.runningServers.put(jobEmulator.getProgramPath(), pr);
        int response = pr.waitFor();
        log.info("CHILD PROCESS CLOSED: {}", response);
        this.runningServers.remove(pr);
    }

    private String getOS() {
        if (SystemUtils.IS_OS_LINUX) {
            return "linux";
        } else if (SystemUtils.IS_OS_WINDOWS) {
            return "windows";
        }
        return "unknown";
    }

    @PreDestroy
    public void destroyCallback() {
        this.runningServers.values().stream().forEach(process -> {
            process.destroyForcibly();
        });
    }

}

