package com.broker.emulator.xml;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;


@Getter @Setter
@XmlRootElement(name = "emulators")
public class JobConfig {

    private List<JobEmulator> emulator;

}
