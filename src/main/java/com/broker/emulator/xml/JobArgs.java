package com.broker.emulator.xml;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement(name = "arg")
public class JobArgs {
    private String name;
    private String value;
}
