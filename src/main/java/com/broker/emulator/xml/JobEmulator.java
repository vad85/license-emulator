package com.broker.emulator.xml;

import lombok.Data;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.List;

@Data
public class JobEmulator {

    private String programPath;
    private String logPath;
    private int port;
    private String proto;

    private List<JobArgs> args;
    private String sampleCommand;
    private String sampleOutput;

    @XmlElementWrapper(name="args")
    @XmlElement(name="arg")
    public List<JobArgs> getArgs() {
        return args;
    }
}
